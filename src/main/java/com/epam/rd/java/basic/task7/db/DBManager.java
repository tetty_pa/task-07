package com.epam.rd.java.basic.task7.db;

import java.io.FileReader;
import java.io.Reader;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;

	Properties properties = new Properties();
	Connection connection;
	public static synchronized DBManager getInstance() {
		if(instance==null){
			instance =  new DBManager();
		}
		return instance;
	}

	private DBManager() {
		try(Reader reader = new FileReader("./app.properties")) {
			properties.load(reader);
			var url = properties.getProperty("connection.url");
			connection = DriverManager.getConnection(url);
		} catch (Exception e){
			System.out.println("WARNING: app.properties not found");
		}
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		String query = "SELECT * FROM users";
		try (Statement stmt = this.connection.createStatement();
			 ResultSet rs = stmt.executeQuery(query)){
			while (rs.next()){
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setLogin(rs.getString("login"));
				users.add(user);
			}
			//return users;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Data Base Exception", e);
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		String query = "INSERT INTO users (id, login) VALUES (DEFAULT, ?)";
		try(PreparedStatement stmt = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);){
			stmt.setString(1, user.getLogin());
			stmt.executeUpdate();
			try (ResultSet generatedKeys = stmt.getGeneratedKeys();) {
				if (generatedKeys.next()) {
					user.setId(generatedKeys.getInt(1));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Data Base Exception", e);
		}
		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		String query = "DELETE FROM users WHERE login = ? ";
		try(PreparedStatement stmt = connection.prepareStatement(query)){
			for (User u: users) {
				stmt.setString(1, u.getLogin());
				stmt.execute();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Data Base Exception", e);
		}
		return true;
	}

	public User getUser(String login) throws DBException {
		String query = "SELECT * FROM users WHERE login = ?";
		User user = new User();
		try( PreparedStatement stmt=connection.prepareStatement(query);){
			stmt.setString(1, login);
			try ( ResultSet rs = stmt.executeQuery();) {
				if (rs.next()) {
					user.setId(rs.getInt("id"));
					user.setLogin(rs.getString("login"));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Data Base Exception", e);
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		String query = "SELECT * FROM teams WHERE name = ?";
		Team team = new Team();
		try(PreparedStatement stmt= connection.prepareStatement(query);){
			stmt.setString(1, name);
			try ( ResultSet rs = stmt.executeQuery();) {
				while (rs.next()) {
					team.setId(rs.getInt("id"));
					team.setName(rs.getString("name"));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Data Base Exception", e);
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		String query = "SELECT * FROM teams";
		try (Statement stmt = this.connection.createStatement();
			 ResultSet rs = stmt.executeQuery(query)){
			while (rs.next()){
				Team team = new Team();
				team.setId(rs.getInt("id"));
				team.setName(rs.getString("name"));
				teams.add(team);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Data Base Exception", e);
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		String query = "INSERT INTO teams(id, name) VALUES (DEFAULT ,?)";
		try(PreparedStatement stmt = connection.prepareStatement(query,  Statement.RETURN_GENERATED_KEYS)){
			stmt.setString(1, team.getName());
			stmt.executeUpdate();
			try(ResultSet generatedKeys = stmt.getGeneratedKeys();){
				if(generatedKeys.next()){
					team.setId(generatedKeys.getInt(1));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Data Base Exception", e);
		}
		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		String query = "INSERT INTO users_teams VALUES (?,?)";
		try(PreparedStatement stmt = connection.prepareStatement(query)){
			connection.setAutoCommit(false);

			for (Team team: teams) {
				stmt.setInt(1, user.getId());
				stmt.setInt(2, team.getId());
				stmt.execute();
			}
			connection.commit();
			connection.setAutoCommit(true);
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				connection.rollback();
				connection.setAutoCommit(true);
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			throw new DBException("Data Base Exception", e);
		}
		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		String query = "SELECT * FROM users_teams\n" +
				"JOIN teams t on t.id = users_teams.team_id\n"+
				"WHERE user_id =?";
		try (PreparedStatement stmt = connection.prepareStatement(query);){
			stmt.setInt(1, user.getId());
			ResultSet rs = stmt.executeQuery();
			while (rs.next()){
				Team team = new Team();
				team.setName(rs.getString("name"));
				teams.add(team);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Data Base Exception", e);
		}

		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		String query = "DELETE FROM teams WHERE id= ?";
		try(PreparedStatement stmt = connection.prepareStatement(query)){
			stmt.setInt(1, team.getId());
			stmt.execute();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Data Base Exception", e);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		String query = "UPDATE teams SET name = ? WHERE id = ?";
		try(PreparedStatement stmt = connection.prepareStatement(query)) {
			stmt.setString(1, team.getName());
			stmt.setInt(2, team.getId());
			stmt.execute();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Data Base Exception", e);
		}
	}

	public static void main(String[] args) throws Exception {
		var db = DBManager.getInstance();
		//var stmt = db.connection.createStatement();
		//var rs = stmt.execute("TRUNCATE TABLE users");
		User tanya = User.createUser("tanya");
		User mama = User.createUser("mama");
		Team t = Team.createTeam("vfvf");
		System.out.println(db.insertUser(tanya));
		System.out.println(db.insertUser(mama));
		System.out.println(db.insertTeam(t));
		db.setTeamsForUser(tanya, t);
		db.setTeamsForUser(mama, t);
		System.out.println(db.deleteUsers(mama, tanya));
		System.out.println(db.findAllUsers());

		//var rs1 = stmt.execute("TRUNCATE TABLE users");

	}
}
